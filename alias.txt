# Main

r=restart
.=clear
ec=search -u https://www.ecosia.org/search?method=index&q=%
br=url https://search.brave.com/search?q=%&source=web
t=termux trans -no-ansi % 
yt=url https://invidious.lidarshield.cloud/search?q=%
dex=@ sh storage/shared/t-ui/script/dex %
@=termux %
swich=@ sh storage/shared/t-ui/swich_theme.sh
gor=@ python storage/shared/t-ui/script/gor.py %
url=search -u %
lal=cat t-ui/alias.txt
la=apps -ls

###

# Engines
sx=search -u http://searx.be/?q=%
#ec-search -u https://www.ecosia.org/search?method-index&q-% -- replace - w/ equal
#br-url https://search.brave.com/search?q-%&source-web -- replace - w/ equal
wh=url https://farside.link/whoogle/search?q=%
whg=search -u https://search.garudalinux.org/search?q=%

# Youtube
yw=url https://yewtu.be/search?q=%
fl=url https://invidious.flokinet.to/search?q=%
#yt-url https://invidious.lidarshield.cloud/search?q-% -- replace - w/ equal

# Translators/dictionars
#t-termux trans -no-ansi %
#dex-@ sh dex.sh %
dx=search -u dexonline.ro/definitie/%

# Wiki
w=url https://en.m.wikipedia.org/w/index.php?search=%
wro=url https://ro.m.wikipedia.org/w/index.php?search=%
wru=url https://ru.m.wikipedia.org/w/index.php?search=%
dexw=search -u https://en.m.wiktionary.org/wiki/?search=%

# Lingva/gui
lt=search -u https://lingva.ml/auto/%/%
ltt=search -u https://lingva.ml/%/%/%
ro=search -u https://lingva.ml/auto/ro/%
fr=t fr,%
cs=t cs,%
ru=t ru,%
en=t en,%

# Scripts
#gor-@ python programming/gor.py %

# Tui system
dark=cat t-ui/suggestions-dark.xml > t-ui/suggestions.xml && cat t-ui/theme-dark.xml > t-ui/theme.xml && cat t-ui/notifications-dark.xml > t-ui/notifications.xml
light=cat t-ui/suggestions-light.xml > t-ui/suggestions.xml && cat t-ui/theme-light.xml > t-ui/theme.xml && cat t-ui/notifications-light.xml > t-ui/notifications.xml
flashbang=light
#swich-@ sh storage/shared/t-ui/swich_theme.sh
add=alias -add %
aled=open t-ui/alias.txt
edal= tuixt t-ui/alias.txt
notfed=open t-ui/notifications.xml

# Apps search
ps=search -ps %
st=apps -st %

# Urls
#url-search -u %
link=url

# Github
git=search -u https://github.com/search?q=%
github=search -u https://github.com

# Romanian Aliases/ Too lazy
șo=wo %
țt=yt %
ș=w %
țș=yw %

# List
#lal-cat t-ui/alias.txt
#la-apps -ls

# Notes
nouted=echo "%" >> notess.txt && cat notess.txt
notess=cat notess.txt

# Weather
weac=curl -s http://wttr.in/Chisinau?%?0T

# Nonsens
yes=open download/no.png
quo=@ fortune

# Uncategorised

init=export PATH=$PATH:/data/data/com.termux/files/usr/bin/:/data/data/com.termux/files/usr/bin/applets
mpw=su -c sh t-ui/script/mpw %
