#!/bin/python3
import sys
from random import choice


def main():

    if len(sys.argv) > 1: 
        items = sys.argv[1:]
    else: 
        items = sys.stdin.readlines()
        items = [item.strip("\n") for item in items if item != "\n"]
        
        # items = [posib.strip() for posib in input().split(",") ]

    alesu = choice(items)
    print(f"----{'-' * len(alesu)}----\n",
          "--- ", alesu, " ---", 
          f"\n----{'-' * len(alesu)}----",sep="")

if __name__ == "__main__":
    main()
