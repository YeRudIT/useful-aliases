# Aliases

This is a list of my favorite aliases for [T-ui Expert](https://github.com/v1nc/T-UI-Expert) Launcher

# Wiki

## Main

In the `#Main` category are all the aliases displayed without input.
This is specific to my configuration of the launcher.

## Translators/dictionary

These aliases help me translate and look up for the definitions of words.

### Trans

```t=termux trans -no-ansi %``` 
requires a package on termux called `translate-shell`

```sh
pkg install translate-shell
```

### Dex

```dex=@ sh dex.sh``` uses the `/script/dex.sh` from termux to scrape [dexonline](dexonline.ro).

**Note** that this is a Romanian dictionary

## Scripts

### GOR (God Of Randomness)

`gor` alias uses a script in python `/script/gor.py` that chooses a random string from the given ones.

## Tui System

### Mode switcher

There are a list of aliases that changes my launcher's color scheme, by rewriting the theme files with the dark or light one. 

## Romanian aliases

Romanian aliases help me when I am too lazy to switch my keyboard.

## Nonsens

Just Nonsense.

## Uncategorised 

### MPv Watch

The alias uses a script `script/mpw` that opens the given video url in [Mpv](https://github.com/mpv-android/mpv-android/pull/58). **Note that you should use mpv version that supports youtube-dl**

