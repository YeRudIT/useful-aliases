#!/bin/bash

# This script switches your t-ui theme from dark to light
# Note: the script is in "development"

if  grep light-mode /data/data/com.termux/files/home/storage/shared/t-ui/theme.xml  
then
    cat $HOME/storage/shared/t-ui/suggestions-dark.xml > $HOME/storage/shared/t-ui/suggestions.xml 
    cat $HOME/storage/shared/t-ui/theme-dark.xml > $HOME/storage/shared/t-ui/theme.xml
    cat $HOME/storage/shared/t-ui/notifications-dark.xml > $HOME/storage/shared/t-ui/notifications.xml
    termux-wallpaper -f $HOME/storage/shared/t-ui/I-use-arch-wallpaper-btw.jpg
else
    cat $HOME/storage/shared/t-ui/suggestions-light.xml > $HOME/storage/shared/t-ui/suggestions.xml 
    cat $HOME/storage/shared/t-ui/theme-light.xml > $HOME/storage/shared/t-ui/theme.xml
    cat $HOME/storage/shared/t-ui/notifications-light.xml > $HOME/storage/shared/t-ui/notifications.xml
    termux-wallpaper -f $HOME/storage/shared/t-ui/I-use-light-mode-btw.png
fi

echo now restart

